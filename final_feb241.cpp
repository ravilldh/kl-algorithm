#include <iostream>
#include <vector>
#include <map>
#include <fstream>
#include <algorithm>
#include <ctime>
#include <cstdlib>


//Output.open(R1);
using namespace std;
ofstream Output_B1("R1");
typedef vector<int> Vectors;
typedef multimap<int, int> Maps;

Maps test, part1, part2, dPart1, dPart2;
Maps :: iterator mapIter;

    ofstream Output;Vectors initPart1, initPart2, mV1, mV2, cutsetVec;
Vectors :: iterator vecIter;


// Function to copy a vector, src into another vector, v

void copyVector(vector<int> src, vector<int> &v) {
    for(vecIter = src.begin(); vecIter != src.end(); vecIter++)
	v.push_back(*vecIter); 
}


// Function for printing a vector

void vecPrint(vector<int> &printVec) {
    for(vecIter = printVec.begin(); vecIter != printVec.end(); vecIter++) 
	    Output_B1 << *vecIter << " " ;
}


// Function for printing a multimap

void mapPrint(multimap<int, int> &printMap) {
    for(mapIter = printMap.begin(); mapIter != printMap.end(); mapIter++)
        cout << mapIter -> first << "=>" << mapIter -> second << "; ";
}


// Function for finding an element, "key" in a vector "Vec"
int findInVec(vector<int> &Vec, int key) {
    if(std::find(Vec.begin(), Vec.end(), key) != Vec.end())
	return 1;
    else
	return 0;         
}


// Initial Partition Using "random_shuffle" in C++
// First n/2 nodes in Partition1 and other n/2 nodes in Partition2: initPart1, initPart2

void initialPartition(int nodes, vector<int>& initPart1, vector<int>& initPart2) {
    std::srand ( unsigned ( std::time(0) ) );
    std::vector<int> myvector;

    for(int i=1; i<=nodes; ++i) myvector.push_back(i); 
    std::random_shuffle ( myvector.begin(), myvector.end() );

    for(int i = 0; i<myvector.size()/2; i++)
	  initPart1.push_back(myvector[i]);	
    for(int i = myvector.size()/2; i<myvector.size(); i++)
	  initPart2.push_back(myvector[i]);		    
}

// Based on the above two partitions, connected nodes are stored in two multimaps: part1, part2

void mapPartition(multimap<int, int> &mapPart, vector<int>& initPart, multimap<int, int> &oMap) {
    for(vecIter = initPart.begin(); vecIter != initPart.end(); vecIter++) {
	int key = *vecIter;
	std::pair <Maps::iterator, Maps::iterator> ret;
	ret = oMap.equal_range(key);
	for(mapIter = ret.first; mapIter!=ret.second; ++mapIter) 
	    mapPart.insert(std::pair<int,int>(key, mapIter->second));
    }
}


// D = External Connections - Internal Connections, cutset = External Connections
// Function to find D for a multimap, part
// Two partitioned vectors(initPart1, initPart2), two multimaps (part1, part2) and 
// a variable cutset, to store cutset value are sent as arguments

void computeD(multimap<int, int> &part, vector<int> &initPart1, vector<int> &initPart2, 
				multimap<int, int> &part1, multimap<int, int> &part2, int &cutset) {
    cutset = 0;
    std::pair <Maps::iterator, Maps::iterator> it;
    for(vecIter = initPart1.begin(); vecIter != initPart1.end(); vecIter++) {
	int key = *vecIter;
	int E, I;
	E = 0; I = 0;
	it = part1.equal_range(key);
	for(mapIter = it.first; mapIter != it.second; mapIter++) {
//            it = part1.equal_range(key);		
	    if(findInVec(initPart2,mapIter->second))
	        E = E + 1;	    
	    else
	        I = I + 1;
	}
	for(mapIter = part1.begin(); mapIter != part1.end(); mapIter++) {
/*	    if(mapIter -> first == key) {
	        if(findInVec(initPart2, mapIter->second))
		    E = E + 1;		
		else 
		    I = I + 1;		
	    }*/
	    if(mapIter -> second == key) {
	        if(findInVec(initPart2, mapIter->first))
		    E = E + 1;
		else 
		    I = I + 1;		
	    }
	}
	for(mapIter = part2.begin(); mapIter != part2.end(); mapIter++) {
	    if(mapIter -> second == key) {
	        if(findInVec(initPart2, mapIter->first))
		    E = E + 1;		
		else 
		    I = I + 1;		
	    }
/*	    if(mapIter -> first == key) {
		if(findInVec(initPart2, mapIter->second))
		    E = E + 1;		
		else 
		    I = I + 1;
	    }*/
	}
	it = part2.equal_range(key);
	for(mapIter = it.first; mapIter != it.second; mapIter++) {
         //   it = part2.equal_range(key);		
	    if(findInVec(initPart2,mapIter->second))
	        E = E + 1;	    
	    else
	        I = I + 1;
	}
	part.insert(std::pair<int,int>(key, E-I));
	cutset = cutset + E;
	
    }
}


// C(i, j)
// No. of connections between i and j are calculated in the below function

int Cab(int a, int b) {
    int c;
    c = 0;    
    std::pair <Maps::iterator, Maps::iterator> it;
    if(findInVec(mV1, a) && findInVec(mV2, b)) {
/*	for(mapIter = part1.begin(); mapIter != part1.end(); mapIter++) {
	    if(mapIter -> first == a && mapIter->second == b) 
		c = c + 1;    	    
	}*/
	it = part1.equal_range(a);
	for(mapIter = it.first; mapIter != it.second; mapIter++) {
	    if(mapIter->second == b)
	        c = c+1;	    
	}
/*        for(mapIter = part2.begin(); mapIter != part2.end(); mapIter++) {
	    if(mapIter->second == a && mapIter->first == b) 
	        c = c + 1;	    
	}*/
	it = part2.equal_range(b);
	for(mapIter = it.first; mapIter != it.second; mapIter++) {
	    if(mapIter->second == a)
	        c = c+1;	    
	}
    }
    if(findInVec(mV2, a) && findInVec(mV1, b)) {
	/*for(mapIter = part2.begin(); mapIter != part2.end(); mapIter++) {
	    if(mapIter -> first == a && mapIter->second == b) 
		c = c + 1;		
	}*/
	it = part2.equal_range(a);
	for(mapIter = it.first; mapIter != it.second; mapIter++) {
	    if(mapIter->second == b)
	        c = c+1;	    
	}    
/*        for(mapIter = part1.begin(); mapIter != part1.end(); mapIter++) {
            if(mapIter->second == a && mapIter->first == b) 
                c = c + 1;	    	    
        }*/
	it = part1.equal_range(b);
	for(mapIter = it.first; mapIter != it.second; mapIter++) {
	    if(mapIter->second == a)
	        c = c+1;	    
	}
    }
    if(findInVec(mV1,a) && findInVec(mV1,b)) {
/*        for(mapIter = part1.begin(); mapIter != part1.end(); mapIter++) {
	    if(mapIter->first == a && mapIter->second == b) 
		c = c+1;		    
	}*/
	it = part1.equal_range(a);
	for(mapIter = it.first; mapIter != it.second; mapIter++) {
	    if(mapIter->second == b)
	        c = c+1;	    
	}    
/*	for(mapIter = part1.begin(); mapIter != part1.end(); mapIter++) {
	    if(mapIter->second == a && mapIter->first == b) 
		c = c + 1;	    
	}*/
	it = part1.equal_range(b);
	for(mapIter = it.first; mapIter != it.second; mapIter++) {
	    if(mapIter->second == a)
	        c = c+1;	    
	}
    }
    if(findInVec(mV2,a) && findInVec(mV2,b)){
/*        for(mapIter = part2.begin(); mapIter != part2.end(); mapIter++) {
	    if(mapIter->first == a && mapIter ->second == b) 
	        c = c+1;	    	    
	}*/
	it = part2.equal_range(a);
	for(mapIter = it.first; mapIter != it.second; mapIter++) {
	    if(mapIter->second == b)
	        c = c+1;	    
	}
/*	for(mapIter = part2.begin(); mapIter != part2.end(); mapIter++) {
	    if(mapIter->second == a && mapIter ->first == b) 
	        c = c+1;	    	    
	}*/
	it = part2.equal_range(b);
	for(mapIter = it.first; mapIter != it.second; mapIter++) {
	    if(mapIter->second == a)
	        c = c+1;	    
	}
    }	
    return c;
}


// GainPair
// Class is created to store a pair of nodes, which goes to the gain queue

class GainPair {
    public:
	    int num1;
	    int num2;
	    int netGain;
	    GainPair(int p1, int p2, int gain) {
	        num1 = p1;
	        num2 = p2;
	        netGain = gain;	
	    }
};

typedef vector<GainPair> GainQueue;


// Function to choose a pair, which gives maximum gain

void selPair(GainPair& gi, vector<int> &sV1, vector<int> &sV2) {
    int gab;
    for(vecIter = sV1.begin(); vecIter != sV1.end(); vecIter++) {
        for(Vectors :: iterator it = sV2.begin(); it != sV2.end(); it++) {
	    gab = (dPart1.find(*vecIter) -> second) + (dPart2.find(*it) -> second) - (2 * Cab(*vecIter, *it));	
	    if(gab > gi.netGain) {
	        gi.num1 = *vecIter;
		gi.num2 = *it;
		gi.netGain = gab;
	    }	    
	}
    }
}


// Function to lock a pair of nodes (p1, p2), which is swapped

void lockPair(int p1, int p2) {
    if(findInVec(initPart1, p1))
 	initPart1.erase(std::remove(initPart1.begin(), initPart1.end(), p1), initPart1.end());
    if(findInVec(initPart2, p2))
 	initPart2.erase(std::remove(initPart2.begin(), initPart2.end(), p2), initPart2.end());
}


// Function to find the nodes, which are connected to the pair (p1, p2)

void ConnectingNodes(GainPair& gi, vector<int>& udv) {
    std::pair <Maps::iterator, Maps::iterator> ret;
    ret = part1.equal_range(gi.num1);
    for(mapIter = ret.first; mapIter!=ret.second; ++mapIter) 
        udv.push_back(mapIter->second);    
    for(mapIter = test.begin(); mapIter != test.end(); mapIter++) {          
        if(mapIter -> second == gi.num1)
            udv.push_back(mapIter->first);	
    }	
    ret = part2.equal_range(gi.num2);
    for(mapIter = ret.first; mapIter!=ret.second; ++mapIter) 
        udv.push_back(mapIter->second);       
    for(mapIter = test.begin(); mapIter != test.end(); mapIter++) {          
        if(mapIter -> second == gi.num2)
            udv.push_back(mapIter->first);
    }			
}


// Update D values of the nodes which are connected to the pair (p1, p2)

void updateD(GainPair& gi, vector<int>& udVec) {    
    Vectors v2;
    int dum;
    dum = 0;
    int count, d;
    count = 0;
    for(int c = 0; c < udVec.size(); c++) {	
	for(d = 0; d<count; d++) {
            if(udVec[c] == v2[d]) {
		break;
	    }
	}
	if(d==count) {
	    v2.push_back(udVec[c]);
		count++;
	}
    }
	
    for(vecIter = v2.begin(); vecIter != v2.end(); vecIter++) {
	if(findInVec(mV1, *vecIter)) {
	    if(findInVec(mV1, gi.num1)) {
		Maps::iterator it = dPart1.find(*vecIter);
                if(it != dPart1.end()) {
	            it -> second = dPart1.find(*vecIter) -> second + (2 * Cab(*vecIter, gi.num1)) - (2 * Cab(*vecIter, gi.num2));
		}
	    }
	    else {
		Maps::iterator it = dPart1.find(*vecIter);
                if(it != dPart1.end()) {
	            it -> second = dPart1.find(*vecIter) -> second + (2 * Cab(*vecIter, gi.num2)) - (2 * Cab(*vecIter, gi.num1));
		}
	    }
	}
	else {
	    if(findInVec(mV2, gi.num1)) {
		Maps::iterator it = dPart2.find(*vecIter);
                if(it != dPart2.end()) {
	            it -> second = dPart2.find(*vecIter) -> second + (2 * Cab(*vecIter, gi.num1)) - (2 * Cab(*vecIter, gi.num2));
		}
	    }
	    else {
		Maps::iterator it = dPart2.find(*vecIter);
                if(it != dPart2.end()) {
	            it -> second = dPart2.find(*vecIter) -> second + (2 * Cab(*vecIter, gi.num2)) - (2 * Cab(*vecIter, gi.num1));
		}
	    }
	}
    }	
}



int main() {
    int nodes, edges;
    int a, b;
    int gab;
    int n;
    char filename[20];
    int currentGain, mtotalGain, mtgIndex;
    currentGain = 0;
    int cutset;
    int k=0;

    ofstream Output_B1("R1");

    Vectors udVec; 
    GainQueue queue;
    ifstream benchmark;
    cin.getline(filename, 20);
    benchmark.open(filename);
    
    if(!benchmark.is_open()) {
        cout << " No such file exists" << endl << endl;
	return 0;
    }
    benchmark >> nodes;
    benchmark >> edges;
    while(benchmark >> a >> b) 
        test.insert(std::pair<int,int>(a,b));

    cout << " Completed Reading Data from the Benchmark. " << endl << endl;

    initialPartition(nodes, initPart1, initPart2);
	
    cout << " Initial Partition is Done, Partitioning Maps, Computing D Values and GainQueue...." << endl << endl;

    for(int gen = 0; gen < 5; gen++) {

        copyVector(initPart1, mV1);
        copyVector(initPart2, mV2);

        n = nodes/2;
        mapPartition(part1, initPart1, test);
    	mapPartition(part2, initPart2, test);    


	computeD(dPart1, initPart1, initPart2, part1, part2, cutset); // computeD for Part1
	computeD(dPart2, initPart2, initPart1, part2, part1, cutset); // computeD for Part2  
	cout << endl <<" Cutset for Iteration #" << gen << " is " << cutset << endl << endl;
	    
    	for(int iter = 0; iter < n; iter++) { 
            GainPair gi(0, 0, 0);
            selPair(gi, initPart1, initPart2);
  	
	    if(gi.num1 == 0 && gi.num2 ==0) {
		k = 1;    
                break;	 
	    }	

            lockPair(gi.num1, gi.num2);

	    queue.push_back(gi);
	    cout << endl << "Gain Queue: Iteration # " << iter << "(" << gi.num1 << "," << gi.num2 << ") with a gain of"  << gi.netGain << endl;
	    currentGain += gi.netGain;

	    if(currentGain > mtotalGain) {
	        mtotalGain = currentGain;
	        mtgIndex = iter;
	    }	    

	    if(iter + 1 < n) {
                ConnectingNodes(gi, udVec);  
    	        updateD(gi, udVec);
	    }

	    udVec.clear();
        } 
	

        cout << endl;
        for(int i = 0; i < queue.size(); i++) {
            GainPair gi = queue[i];
	  //  cout << " (" << gi.num1 << "," << gi.num2 << ") with a gain of " << gi.netGain << endl;
            initPart1.push_back(gi.num2);
            initPart2.push_back(gi.num1);
        }

//	if(k) {
//             cutset = cutset - mtotalGain;
//	     break;
//	}
	
	// Clearing All the Maps and Gain Queue for the next Iteration	

        dPart1.clear();
        dPart2.clear();
        queue.clear();
        part1.clear();
        part2.clear();
    }
    Output_B1 << cutset;
    Output_B1 << '\n' ;
    for(int i = 0; i < initPart1.size(); i++)
	    Output_B1 << initPart1[i] << " ";
    Output_B1 << endl;
    for(int i = 0; i < initPart2.size(); i++)
	    Output_B1 << initPart2[i] << " ";
    Output_B1 << endl;

}




